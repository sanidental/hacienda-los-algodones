<?php

$myemail = 'info@sanidentalgroup.com'; //<-----Put Your email address here.
$bccmail = ''; //<-----Put Your BCC email address here.

$name = $_POST['name'];
$email_address = $_POST['email_address'];
$gender = $_POST['gender'];
$phone = $_POST['phone'];
$desired_treatment = $_POST['desired_treatment'];
$desired_appointment = $_POST['desired_appointment'];
$desired_time = $_POST['desired_time'];
$message = $_POST['message'];

// Hidden
$fieldHidden = isset($_POST['elAddress']) ? $_POST['elAddress'] : null;

if($name && $email_address && $phone)
{
	$to = $myemail;
	$email_subject = "Contact from Hacienda Los Algodones - Promotion: $name";
	$email_body = "You have received a new message of Hacienda Los Algodones. Permanent Lodging Promotion".
	" Here are the details:\n
	Name: $name \n
	Email: $email_address \n
	Gender: $gender \n
	Phone: $phone \n
	Desire Dental Treatment: $desired_treatment \n
	Desire Appointment Date: $desired_appointment \n
	Desire Time: $desired_time \n
	Room: $message \n
	";

	$headers = "From: $email_address\n"; /*Campo del Email del cliente*/
	$headers .= "Reply-To: $email_address\n"; /*Campo del Email de respuesta*/
	$headers .= "Bcc: " . $bccmail; /*Campo del Email de copia oculta*/
	if(!$fieldHidden)
	mail($to, $email_subject, $email_body, $headers);
}
//redirect to the 'thank you' page
header('Location: book-a-room-thanks.html');
?>