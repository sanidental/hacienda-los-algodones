<?php

$myemail = 'reservations@haciendalosalgodones.com'; //<-----Put Your email address here.
$bccmail = 'smt.dev.2019@gmail.com'; //<-----Put Your BCC email address here.

$firstName = $_POST['first_name'];
$lastName = $_POST['last_name'];
$emailAddress = $_POST['email_address'];
$phone = $_POST['phone'];
$country = $_POST['country'];
$residence = $_POST['residence'];

// Checkbox
$service = $_POST['service'];
$cleaning = $_POST['cleaning'];
$location = $_POST['location'];
$sleepQuality = $_POST['sleep_quality'];
$quality_rooms = $_POST['quality_rooms'];
$value = $_POST['value'];

$review = $_POST['review'];
// Hidden
$fieldHidden = isset($_POST['elAddress']) ? $_POST['elAddress'] : null;

if($firstName && $lastName && $emailAddress && $review)
{
	$to = $myemail;
	$email_subject = "Review for: $firstName $lastName";
	$email_body = "You have a review from $firstName $lastName".
	" Here are the details:\n
	Review for: $firstName $lastName \n
	Email: $emailAddress \n
	Phone: $phone \n
	Country: $country \n
	Residence: $residence \n
	Your opinion:\n
	$service, $cleaning, $location, $sleepQuality, $quality_rooms, $value \n
	Review: $review \n
	";

	$headers = "From: $emailAddress\n"; /*Campo del Email del cliente*/
	$headers .= "Reply-To: $emailAddress\n"; /*Campo del Email de respuesta*/
	$headers .= "Bcc: " . $bccmail; /*Campo del Email de copia oculta*/
	if(!$fieldHidden)
	mail($to, $email_subject, $email_body, $headers);
}
//redirect to the 'thank you' page
header('Location: complaint-thanks.html');
?>
