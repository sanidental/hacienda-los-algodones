<?php

$myemail = 'reservations@haciendalosalgodones.com'; //<-----Put Your email address here.
$bccmail = 'smt.dev.2019@gmail.com'; //<-----Put Your BCC email address here.

$name = $_POST['name'];
$email_address = $_POST['email_address'];
$phone = $_POST['phone'];
$check_in = $_POST['check_in'];
$check_out = $_POST['check_out'];
$party = $_POST['party'];
$room = $_POST['room'];
$add_guest1 = $_POST['add_guest1'];
$add_guest2 = $_POST['add_guest2'];
$add_guest3 = $_POST['add_guest3'];

// Hidden
$fieldHidden = isset($_POST['elAddress']) ? $_POST['elAddress'] : null;

if($name && $email_address && $phone)
{
	$to = $myemail;
	$email_subject = "Reservation for: $name";
	$email_body = "You have a reservation inquiry from $name".
	" Here are the details:\n
	Reservation for: $name \n
	Email: $email_address \n
	Phone: $phone \n
	Check in: $check_in \n
	Check out: $check_out \n
	Party: $party \n
	Type of room: $room \n
	Additional Guest: $add_guest1\n
	Additional Guest: $add_guest2\n
	Additional Guest: $add_guest3\n
	";

	$headers = "From: $email_address\n"; /*Campo del Email del cliente*/
	$headers .= "Reply-To: $email_address\n"; /*Campo del Email de respuesta*/
	$headers .= "Bcc: " . $bccmail; /*Campo del Email de copia oculta*/
	if(!$fieldHidden)
	mail($to, $email_subject, $email_body, $headers);
}
//redirect to the 'thank you' page
header('Location: book-a-room-thanks.html');
?>
