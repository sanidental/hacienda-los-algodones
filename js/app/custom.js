$(document).foundation();
$(document).ready(function(){
  //topbar
  $('.top-bar ul.right li a').click(function() {
       $('.top-bar').removeClass('expanded');
    });

	//Parallax
  $('.intro-section').parallax("50%", 0.1);
  $('.room-parallax').parallax("50%", 0.1);
  $('.restaurant-parallax').parallax("50%", 0.4);
  $('.gallery-parallax').parallax("50%", 0.3);
  $('.location-parallax').parallax("50%", 0.3);

  //ScrollTo
  $(".scrollto").click(function() {
    $.scrollTo($($(this).attr("href")), {
      duration: 1000,
      offset:-78
    });
    return false;
  });

   //contact
	$( "#button" ).click(function() {
	  $( ".book-a-room" ).toggle( "blind", 500 );
	  return false;
	});

  //datepicker
  $(function() {
    $( "#CheckIn" ).datepicker({dateFormat: "d MM, yy"});
    $( "#CheckOut" ).datepicker({dateFormat: "d MM, yy"});
    $( "#DesireTreatment" ).datepicker({dateFormat: "d MM, yy"});
    $( "#DesireAppointment" ).datepicker({dateFormat: "d MM, yy"});
  });

  // Guest Satisfaction Survey
  ////////////////////////////////////////

  var form = $("#survey-form");

  form.validate();

  form.children("div").steps({
      headerTag: "h3",
      bodyTag: "section",
      transitionEffect: "fade",
      onStepChanging: function (event, currentIndex, newIndex)
      {
          form.validate().settings.ignore = ":disabled,:hidden";
          return form.valid();
      },
      onFinishing: function (event, currentIndex)
      {
          form.validate().settings.ignore = ":disabled";
          return form.valid();
      },
      onFinished: function (event, currentIndex)
      {
      },

      labels: {
        finish: "Submit"
      }
  });

  // Show Questions
  $('#Question1 input[type="radio"]').click(function() {
    if($(this).attr('id') == 'showMe1') {
      $('#showQuestion1').show();           
    }
    else {
      $('#showQuestion1').hide();   
    }
  });

  $('#Question2 input[type="checkbox"]').click(function() {
    if($(this).attr('id') == 'showMe2') {
      $('#showQuestion2').show();           
    }
    else {
      $('#showQuestion2').hide();   
    }
  });

  $('#Question3 input[type="checkbox"]').click(function() {
    if($(this).attr('id') == 'showMe3') {
      $('#showQuestion3').show();           
    }
    else {
      $('#showQuestion3').hide();   
    }
  });

  $('#Question4 input[type="radio"]').click(function() {
    if($(this).attr('id') == 'showMe4') {
      $('#showQuestion4').show();           
    }
    else {
      $('#showQuestion4').hide();   
    }
  });

  $('#Question5 input[type="radio"]').click(function() {
    if($(this).attr('id') == 'showMe5') {
      $('#showQuestion5').show();           
    }
    else {
      $('#showQuestion5').hide();   
    }
  });

  $('#Question6 input[type="radio"]').click(function() {
    if($(this).attr('id') == 'showMe6') {
      $('#showQuestion6').show();           
    }
    else {
      $('#showQuestion6').hide();   
    }
  });

  $('#Question7 input[type="radio"]').click(function() {
    if($(this).attr('id') == 'showMe7') {
      $('#showQuestion7').show();           
    }
    else {
      $('#showQuestion7').hide();   
    }
  });

  $('#Question8 input[type="radio"]').click(function() {
    if($(this).attr('id') == 'showMe8') {
      $('#showQuestion8').show();           
    }
    else {
      $('#showQuestion8').hide();   
    }
  });

  // Hide number
  $('.number').addClass('hide');
        $('.actions li:last-child a[role=menuitem]').css('display', 'none');
        $('.actions li:last-child').append('<button type="submit">boton</button>');


})

